//! Cli argument parsing.

use std::path::PathBuf;

use clap::Parser;

#[derive(Parser, Debug, Clone)]
#[clap(author, version, about, long_about = None)]
pub struct Args {
    /// The fragment shader file to run.
    #[clap(value_parser)]
    pub file: PathBuf,
    /// Width of the window.
    #[clap(short, long, default_value_t = 480)]
    pub width: u32,
    /// Height of the window.
    #[clap(short, long, default_value_t = 480)]
    pub height: u32,
    /// Spawn window incapable of resizing
    #[clap(short = 'r', long, action=clap::ArgAction::SetFalse)]
    pub no_resize: bool,
    /// FPS the shader will run at.
    #[clap(short, long, default_value_t = 60)]
    pub fps: usize,

    /// File to save the output, only the first frame will be saved.
    #[clap(short, long)]
    pub output: Option<PathBuf>,
}
