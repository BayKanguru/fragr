use std::time;

use clap::Parser;
use glium::{
    glutin, implement_vertex,
    index::{NoIndices, PrimitiveType},
    uniform, DrawParameters, Surface, VertexBuffer,
};

use fragr::{cli, gl};

fn main() {
    let args = cli::Args::parse();
    let program_start = time::Instant::now();
    let mut frame_index = 0;
    let mut cursor_position = [0.0, 0.0];
    let mut mouse_state = [0.0, 0.0, 0.0, 0.0]; // left, middle, right, <any other mouse key>

    let event_loop = glutin::event_loop::EventLoop::default();
    let display = gl::create_window(
        &args.file.to_str().unwrap(),
        (args.width, args.height),
        &event_loop,
        args.no_resize,
    )
    .expect("couldn't create window");
    let program = gl::load_shader(&display, &args.file).expect("couldn't load shader");

    let vertices = vec![
        Vertex { position: [-1, -1] },
        Vertex { position: [1, -1] },
        Vertex { position: [1, 1] },
        Vertex { position: [-1, 1] },
    ];
    let vertex_buffer = VertexBuffer::new(&display, &vertices).unwrap();
    let indices = NoIndices(PrimitiveType::TriangleFan);

    event_loop.run(move |event, _, control_flow| {
        *control_flow = match event {
            glutin::event::Event::WindowEvent { event, .. } => match event {
                glutin::event::WindowEvent::CloseRequested => {
                    glutin::event_loop::ControlFlow::ExitWithCode(0)
                }
                glutin::event::WindowEvent::CursorMoved { position, .. } => {
                    cursor_position = [position.x as f32, position.y as f32];
                    glutin::event_loop::ControlFlow::Poll
                }
                glutin::event::WindowEvent::MouseInput { state, button, .. } => {
                    let state = match state {
                        glutin::event::ElementState::Pressed => 1_f32,
                        glutin::event::ElementState::Released => 0_f32,
                    };
                    let button = match button {
                        glutin::event::MouseButton::Left => 0,
                        glutin::event::MouseButton::Middle => 1,
                        glutin::event::MouseButton::Right => 2,
                        _ => 3,
                    };
                    mouse_state[button] = state;
                    glutin::event_loop::ControlFlow::Poll
                }
                _ => glutin::event_loop::ControlFlow::Poll,
            },
            _ => glutin::event_loop::ControlFlow::Poll,
        };

        let next_frame_time = time::Instant::now()
            + time::Duration::from_micros(
                (1_000_000_000 / args.fps)
                    .try_into()
                    .expect("FPS value can not be converted int u64"),
            );
        *control_flow = glutin::event_loop::ControlFlow::WaitUntil(next_frame_time);

        let mut frame = display.draw();
        frame.clear_color(0.0, 0.0, 0.0, 0.0);
        frame
            .draw(
                &vertex_buffer,
                &indices,
                &program,
                &uniform! {
                    time: program_start.elapsed().as_secs_f32(),
                    frame: frame_index,
                    mouse: cursor_position,
                    mouse_state: mouse_state,
                    resolution: [args.width as f32, args.height as f32],
                },
                &DrawParameters::default(),
            )
            .expect("couldn't draw");
        frame.finish().unwrap();
        frame_index += 1;
    })
}

#[derive(Clone, Copy)]
pub struct Vertex {
    position: [i8; 2],
}
implement_vertex!(Vertex, position);
