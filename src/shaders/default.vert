#version 330

in vec2 position;
out vec2 texCoord;

void main() {
    texCoord = position;
    gl_Position = vec4(position, 0, 1);
}
