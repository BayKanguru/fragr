//! High-level glium interface.

use std::{io::Read, path::Path};

use glium::{
    backend::glutin::DisplayCreationError, glutin, Display, Program, ProgramCreationError,
};

pub fn create_window<T>(
    title: &str,
    size: (u32, u32),
    event_loop: &glutin::event_loop::EventLoop<T>,
    resizable: bool,
) -> Result<Display, DisplayCreationError> {
    let wb = glutin::window::WindowBuilder::new()
        .with_inner_size(glutin::dpi::LogicalSize::new(size.0, size.1))
        .with_title(title)
        .with_resizable(resizable);

    let cb = glutin::ContextBuilder::new();

    Display::new(wb, cb, event_loop)
}

pub fn load_shader(display: &Display, path: &Path) -> Result<Program, ProgramCreationError> {
    // FIXME: Handle errors
    let mut file = std::fs::File::open(path).unwrap();
    let mut fragment_shader = String::new();
    file.read_to_string(&mut fragment_shader).unwrap();

    Program::from_source(
        display,
        include_str!(concat!(
            env!("CARGO_MANIFEST_DIR"),
            "/src/shaders/default.vert"
        )),
        &fragment_shader,
        None,
    )
}
