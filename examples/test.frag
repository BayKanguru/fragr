#version 460

uniform float time;
uniform vec2 mouse;
uniform vec4 mouse_state;
uniform int frame;
uniform vec2 resolution;

in vec2 texCoord;
out vec4 fragColor;

void main() {
    if (texCoord == gl_FragCoord.xy/resolution)
        fragColor = vec4(1.0);
}
