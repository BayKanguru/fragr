# fragr

Run fragment shaders like on [ShaderToy](https://www.shadertoy.com).
This was heavily inspired by [frag](https://github.com/snsinfu/frag) but I intend to build more on this idea.

## Usage

```console
$ fragr path/of/the/fragment_shader.frag
```

Help message can be displayed via:
```console
$ fragr --help
```

### Uniforms

```glsl
#version 430

uniform float time; // time elapsed since start in seconds
uniform int frame; // index of the frame, first frame is 0
uniform vec2 mouse; // coordinates of the cursor, only updates within the window
uniform vec4 mouse_state; // correspondig value is 1.0 if pressed else 0.0: x is left-click, y is middle-click, z is right-click and w is reserved (currently any other mouse button)
uniform vec2 resolution; // screen resolution: x is width, y is height
```

### Attributes

```glsl
#version 430

in vec2 texCoord; // normalized coordinates equal to gl_FragCoord.xy/resolution
out vec4 fragColor; // color of the fragment, in this case the pixel
```

## To-Do

- [x] Display basic pixel shaders
- [ ] Saving frame/frames
- [ ] Exit with q or escape
